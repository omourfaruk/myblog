



<?php


include_once'Base.php';

class Post extends Base{

    public $postId;
    public $postTitle;
    public $description;
    public $dateTime;
    public $userId;
    public $error;
    public $totalArticles   =0;
    public $articlesPerPage = 5;
    public $offset = 0; //from where to start retriving the data
    public $search;
    public $serial;





    public function postInsert()  //function for post insert
    {
      $sql = "INSERT INTO post(title,description) VALUES(

                      '".$this->secured($this->postTitle)."',
                      '".$this->secured($this->description)."'

                      )";

                if(mysqli_query($this->cn,$sql))
                {

                      return true;

                }
                      $this->error = mysqli_error($this->cn);
                      return false;

    }






    public function postListDisplay() //function for post-list display
    {
      $a   = array();


      $sql = "SELECT * FROM post ORDER BY id DESC ";

      $result = mysqli_query($this->cn,$sql);
      $this->totalArticles = mysqli_num_rows($result);

      $sql .= " LIMIT ".$this->offset.",10";
      $result = mysqli_query($this->cn,$sql);



      $r   = mysqli_query($this->cn,$sql);

                while($data = mysqli_fetch_assoc($r))
                {
                  $p = new Post;

                  $p->postId       = $data['id'];
                  $p->postTitle    = $data['title'];
                  $p->description  = $data['description'];
                  $p->dateTime     = $data['date_time'];

                  $a[]=$p;
                }

                return $a;

    }




//function for shortlisted display

public function postShorten($text, $limit=400)
{

  $text = substr($text,0,$limit);
  $text = substr($text,0,strrpos($text," "));
  $text = $text.'.......';
  return $text;

}

public function postTitleShorten($text, $limit=400)
{

  $text = substr($text,0,$limit);
  $text = substr($text,0,strrpos($text," "));
  //$text = $text.'.......';
  return $text;

}


// Pagination List display

                public function PaginationListDisplay()
                {

                  //For counting total no of items i.e. total no. of rows in this case & storing the total valu into $totalArticles

                  $sql  = "SELECT * FROM post ORDER BY id DESC";
                  $result = mysqli_query($this->cn,$sql);
                  $this->totalArticles = mysqli_num_rows($result);

                  $sql .= " LIMIT ".$this->offset.",$this->articlesPerPage";
                  $result = mysqli_query($this->cn,$sql);

                  while($data = mysqli_fetch_array($result))
                  {
                    $obj = new Post;


                    $obj->postId       = $data['id'];
                    $obj->postTitle    = $data['title'];
                    $obj->description  = $data['description'];
                    $obj->dateTime     = $data['date_time'];
                    $obj->userId       = $data['user_id'];


                    $a[] = $obj;

                  }

                  return $a;

                }






//function for single post display


            public function singlePostDisplay() //function for post-list display
            {
              $a   = array();




              $sql = "SELECT * FROM post WHERE id= " .$this->postId;


              $r   = mysqli_query($this->cn,$sql);

                        while($data = mysqli_fetch_assoc($r))
                        {
                          $p = new Post;

                          $p->postId       = $data['id'];
                          $p->postTitle    = $data['title'];
                          $p->description  = $data['description'];
                          $p->dateTime     = $data['date_time'];

                          $a[]=$p;
                        }

                        return $a;

            }



            //function for searching  relate post


            public function searchPost() //function for searching related post
            {
              $a   = array();




              $sql = "SELECT * FROM post WHERE title  OR description LIKE  '%$this->search%'" ;


              $r   = mysqli_query($this->cn,$sql);

                        while($data = mysqli_fetch_assoc($r))
                        {
                          $p = new Post;

                          $p->postId       = $data['id'];
                          $p->postTitle    = $data['title'];
                          $p->description  = $data['description'];

                          $a[]=$p;
                        }

                        return $a;

            }





            //Function for post update

            public function postUpdate()

              {

                  $sql =" UPDATE post SET title = '". $this->secured($this->postTitle). "',description= '".$this->secured($this->description)."'
                          WHERE id = ".$this->postId;

                  if(mysqli_query($this->cn,$sql))
                  {

                    return true;

                  }

                  $this->Error = mysqli_error($this->cn);
                  return false;

              }


              //Function For Delete Post
              public function postDelete()
              {

                      $sql = " DELETE FROM post WHERE id=".$this->postId;

                      if(mysqli_query($this->cn,$sql))
                      {

                          return true;

                      }
                        $this->Error = mysqli_error($this->cn);
                        return false;


              }




}
























?>
