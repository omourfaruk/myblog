<?php

include_once'Base.php';

class Country extends Base
{
  public $countryId;
  public $countryName;
  public $totalArticles   =0;
  public $articlesPerPage;
  public $offset = 0;
  public $serial;

              //FUNCTION for inseting country name into country TokyoTyrantTable

              public function countryInsert()
              {
                $sql = "INSERT INTO country(country_name) VALUES('".$this->secured($this->countryName)."')";


                          if(mysqli_query($this->cn,$sql))
                          {
                            return TRUE;
                          }
                          else
                          {
                            $this->error = mysqli_error($this->cn);
                            return FALSE;
                          }

              }



                //FUNCTION for Displaying country name

                public function countryDisplay()
                {
                  $a   = array();

                  $sql = "SELECT * FROM country";

                  $result = mysqli_query($this->cn,$sql);

                  $this->totalArticles = mysqli_num_rows($result);

                  $sql .= " LIMIT ".$this->offset.",10";



                  $r   = mysqli_query($this->cn,$sql);

                  while($data = mysqli_fetch_assoc($r))
                  {
                    $c = new Country;

                    $c->countryId   = $data['id'];
                    $c->countryName = $data['country_name'];

                    $a[]=$c;
                  }

                  return $a;


                }




                public function countryDelete()
                {
                  $sql = " DELETE FROM country where id=".$this->countryId;

                  if(mysqli_query($this->cn,$sql))
                  {
                    return true;
                  }
                  $this->Error = mysqli_error($this->cn);
                  return false;

                }




                //country update function
                public function countryUpdate()
                {

                      $sql =" UPDATE country SET country_name = '".$this->countryName."' WHERE id = ".$this->countryId;

                          if(mysqli_query($this->cn,$sql))
                          {

                            return true;

                          }

                          $this->Error = mysqli_error($this->cn);
                          return false;

                }



                //function For Selecting Country Name in  City Table

                public function countryList()
                {
                  $list = '<option value="0">Select One</option>';
                  $sql = "SELECT * FROM country ORDER BY country_name ASC";
                  $r   = mysqli_query($this->cn,$sql);

                  while($data = mysqli_fetch_assoc($r))
                  {
                      $list.='<option value="'.$data['id'].'">'.$data['country_name'].'</option>';

                  }

                  return $list;


                }



                //function for selecting Country Name by their id

                public function countryNameById()
                {
                  $sql ="SELECT country_name FROM country WHERE id=".$this->countryId;

                  $r = mysqli_query($this->cn,$sql);

                  while($data=mysqli_fetch_assoc($r))
                  {
                    return $this->countryName = $data['country_name'];
                  }


                }




}

















?>
