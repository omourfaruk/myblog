

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>

    <link type="text/css" rel="stylesheet" href="admin/css/post_edit.css">

  </head>
  <body>


    <?php

    include_once 'db/Post.php';

    $p = new Post;
    $er = 0;
    $epostTitle = "";
    $epostDescription ="";
    $epostEditId = "";

    /*
      * This loop will work only when the Edit button is pressed
      * on the city_display.php page
    */
  //  if(isset($_POST['btn_post_edit']))
  //  {
    //	print $postEditId = $_POST['post_edit_id'];
    //}


    /*
      * This loop will work only when the update button of the
      * current page will be pressed.
    */

        if(isset($_POST['btn_update']))
        {

           $p->postId          =  $_POST['post_id'];
        	 $p->postTitle       =  $_POST['title'];
        	 $p->description     =  $_POST['description'];


                  // when city name input field is empty this loop will work
                  	if( $p->postTitle  == "" )
                  	{
                  		$er++;
                  		$epostTitle = '<span class="error">Insert a title please. </span>';
                  	}

                  // when no country will be seleted this loop will work
                  	if( $p->description   == "")
                  	{
                  		$er++;
                  		$epostDescription = '<span class="error">Insert your post please. </span>';
                  	}


                  // when no error will be occured this loop will work
                  	if($er == 0)
                  	{

                      		if($p->postUpdate())
                      		{

                      			   header('Location:dashboard.php?admin=post_option.php');
                      		}

                      		print $p->Error;


                  	}





        }//end of isset($_POST['btn_update']) if loop
        elseif(isset($_POST['btn_post_edit']))
        {

              //include_once'base/post.php';
              $postEditId = $_POST['post_edit_id'];
              $p->postId  = $postEditId;

              foreach ($p->singlePostDisplay() as $item)
              {

                      $p->postId        = $item->postId;
                      $p->postTitle     = $item->postTitle;
                      $p->description   = $item->description;

              }


      }
      else
      {
        header('Location:dashboard.php?admin=post_option.php');
      }





  ?>



        <h1>POST EDIT PAGE</h1><br>

        <div class="container">

          <form action="" method="post">

            <input type="hidden" name="post_id" value="<?php print $p->postId;?>"/><!--id of the post to be updated-->

            <p>Title</p><!--start of post_title section-->

                <textarea    name="title" placeholder="Write your post title here. " class="post_title"><?php print $p->postTitle ;?></textarea>
                <br/><?php print $epostTitle; ?><br/><br/>
              <!--end of post_title section-->



            <p>Description</p><!--start of post_description section-->

                  <textarea class="post_description" name="description" placeholder="Write your post here. "><?php print $p->description ;?></textarea>
                  <br/><?php print $epostDescription; ?><br/><br/>

              <input type="submit" name="btn_update" value="Update" class="btn_post" onclick= "confirmFunction()" >


          </form>


          <script>

          function confirmFunction() {

              confirm("Are you sure you want to update !");

          }

          </script>

        </div>


  </body>
</html>
