

<link type="text/css" rel="stylesheet" href="admin/css/country_display.css">



<?php

include_once'db/Country.php';

$c = new Country;
$i=0;//initialization for color table

if(isset($_GET['start']))
{
  $c->offset = $_GET['start'];
  $c->serial =$c->offset;
  /*
    *initialization for starting of serial
    *first offset= 0 so serial will be start from 1-10
    * when page 2 will be select offset will be 10
     so serial no of next page will start from 11
  */

}

print'<div class="country-display">';//start of country-display section

print'<h1>Admin Country Table</h1>';


print '<table >';
print '<tr>';
print '<th class="heading_id" > Id </th>';
print '<th class="heading_country" > Country </th>';
print '<th class="heading_delete"> Delete</th>';
print '<th class="heading_update"> Update</th>';
print '</tr>';

foreach ($c->countryDisplay() as $item)
{

   $c->serial++;

  if($i==0)
  {
    print '<tr bgcolor="silver">';
    $i=1;;
  }
  else
  {
    print '<tr>';
    $i=0;
  }

  print '<td>'.$c->serial.'</td>';
  print '<td>'.$item->countryName.'</td>';
  print '<td>

        <form method="post" action="?admin=country_delete.php">
        <input type="hidden" value="'.$item->countryId.'" name="country_delete_id">
        <input type="submit" name="btn_country_delete" value="Delete" class="button_delete"/>
        </form>

        </td>';

  print '<td>

              <form method="post" action="?admin=country_update.php">
              <input type="hidden" value="'.$item->countryId.'" name="country_update_id">
              <input type="submit" name="btn_country_update" value="Update" class="button_update"/>
              </form>

              </td>';

  print '</tr>';

}
print '</table>';

print'</div>';//end of country-display section





//start of pagination

  $l= 0;
  $k= 1;

  print'<div class="country-display-pagination">';

  print '<nav>
    <ul class="pagination">';

    if($c->offset != 0) //i.e. when we are not in the first page this loop will not work i.e. the Previous page sign will not appear
    {
    print '
      <li>
        <a href="?start='.(  $c->offset-10).'" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </a>
      </li>';
    }


  while($l*10   < $c->totalArticles)
  {
    print '<li><a href="?start='.($l* 10).'">'.$k++.'</a></li>'; //$j++ is the post increment operator it first return $j then increment
    $l++;                                                          //by one
  }


  if(($c->offset + 10)  <  $c->totalArticles - ( $c->totalArticles % 10))
  {
    print '
      <li>
        <a href="?start='.($c->offset+10).'" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>';

  }





   print ' </ul>
  </nav>
  ';

  print'</div>';
//end of pagination



?>
