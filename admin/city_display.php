


<link type="text/css" rel="stylesheet" href="admin/css/city_display.css">

<?php

include_once'db/City.php';

$ct = new City;
$j=0; //for color loop


if(isset($_GET['run']))
{
  $ct->offset = $_GET['run'];
  $ct->serial =$ct->offset;
  /*
    *initialization for starting of serial
    *first offset= 0 so serial will be start from 1-10
    * when page 2 will be select offset will be 10
     so serial no of next page will start from 11
  */

}

print'<div class="admin-city-display">';

print '<h2>Admin City Display Table</h2>';


print'<table >';

      print'<tr>';

      print'<th class="heading_id">Id</th>';
      print'<th class="heading_city">City</th>';
      print'<th class="heading_country">Country</th>';
      print '<th class="heading_delete"> Delete</th>';
      print '<th class="heading_update"> Update</th>';

      print'</tr>';

      foreach ($ct->cityDisplay() as $item)
      {
        $ct->serial++;

        if($j==0)
        {
          print'<tr bgcolor="silver">';
          $j=1;
        }
        else
        {
          print'<tr >';
          $j=0;
        }

            print'<td>'.$ct->serial.'</td>';
            print'<td>'.$item->cityName.'</td>';
            print'<td>'.$item->coun->countryName.'</td>';

            print'<td>

		              <form action="?admin=city_edit.php" method="post">
		              <input type="hidden" value="'.$item->cityId.'" name="city_edit_id"/>
		              <input type="submit" value="Edit" name="btn_city_edit" class="button_update" />
		              </form>

		              </td>';

            print'<td>

		              <form action="?admin=city_delete.php" method="post">
		              <input type="hidden" value="'.$item->cityId.'" name="city_delete_id"/>
		              <input type="submit" value="Delete" name="btn_city_delete" class="button_delete" />
		              </form>

		              </td>';

        print'</tr>';


      }



print'</table>';


print'</div>';




//start of pagination

  $l= 0;
  $k= 1;
  print'<div class="city-display-pagination">';
  print '<nav>
    <ul class="pagination">';

    if($ct->offset != 0) //i.e. when we are not in the first page this loop will not work i.e. the Previous page sign will not appear
    {
    print '
      <li>
        <a href="?run='.(  $ct->offset-10).'" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </a>
      </li>';
    }


  while($l*10   < $ct->totalArticles)
  {
    print '<li><a href="?run='.($l* 10).'">'.$k++.'</a></li>'; //$j++ is the post increment operator it first return $j then increment
    $l++;                                                          //by one
  }


  if(($ct->offset + 10)  <  $ct->totalArticles - ( $ct->totalArticles % 10))
  {
    print '
      <li>
        <a href="?run='.($ct->offset+10).'" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>';

  }





   print ' </ul>
  </nav>
  ';

  print'</div>';
//end of pagination






?>
