


    <?php

    include_once 'db/City.php';

    $ci = new City;
    $er = 0;
    $ecityName = "";
    $ecountryid = "";

    /*
      * This loop will work only when the Edit button is pressed
      * on the city_display.php page
    */
    if(isset($_POST['btn_city_edit']))
    {
    	$cityEditId = $_POST['city_edit_id'];
    }


    /*
      * This loop will work only when the update button of the
      * current page will be pressed.
    */

        if(isset($_POST['btn_update']))
        {

        	 $ci->cityName    = $_POST['city_name'];
        	 $ci->cityId      = $_POST['city_id'];
        	 $ci->countryId   = $_POST['country_id'];

                  // when city name input field is empty this loop will work
                  	if($ci->cityName == "")
                  	{
                  		$er++;
                  		$ename = '<span class="error">Enter your city name please. </span>';
                  	}

                  // when no country will be seleted this loop will work
                  	if($ci->countryId == 0)
                  	{
                  		$er++;
                  		$ecountryid = '<span class="error">Select a country please. </span>';
                  	}

                  // when no error will be occured this loop will work

                  	if($er == 0)
                  	{

                      		if($ci->cityUpdate())
                      		{

                      			   header('Location:dashboard.php?admin=city_display.php');
                      		}

                      		print $ci->Error;


                  	}





        }//end of isset($_POST['btn_update']) if loop
        elseif(isset($_POST['btn_city_edit']))
        {

        $cityEditId = $_POST['city_edit_id'];
        $ci->cityId = $cityEditId;
        $ci->cityNameById();







        }
        else
        {
          header('Location:dashboard.php?admin=city_display.php');
        }





    ?>









<div class="city_page"><!--start of city_edit form-->

              <form action="" method="post">

                                  <label>City Name</label><br/><br/>

                                  <input type="hidden" name="city_id" value="<?php print $cityEditId;?>"/><!--id of the city tobe
                                                                                                            updated -->
                                  <!--Name of the city to be updated -->

                                  <input type="text" name="city_name" placeholder="City Name" value="<?php print $ci->cityName;?>"/>

                                  <?php print $ecityName;?>
                                  <br/>

                                  <label>Country Name</label><br/><br/>

                                  <select name="country_id"><!--id of the country belogs to the city to be updated-->

                                    <option value="<?php print $ci->countryId?>">
                                      <!-- id of country id to be selected-->
                                      <?php

                                      include_once'db/Country.php';

                                      $c = new Country;

                                      $c->countryId = $ci->countryId;

                                       print $c->countryNameById();


                                     ?>
                                   </option>








                                  </select>

                                  <?php print $ecountryid;?>


                                  <br/></br>

                                  <input type="submit" name="btn_update" value="Update"/>


              </form>


</div><!--End of city_edit form-->
