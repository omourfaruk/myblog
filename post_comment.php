

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>jQuery Ajax Comment System - Demo</title>
  <link rel="stylesheet" href="css/index.css">
	<link rel="stylesheet" href="css/post_comment.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="js/post_comment.js"></script>

</head>
<body>

  <?php
  //inclusion of menu-bar header section
   include_once'include/header.php';

   ?>

<div class="body-post-comment"><!--start of body-post-comment-->



		<div class="wrap"><!--start of wrap section-->





							<?php

								// retrive post from database post table by post id





							include_once'db/Post.php';

							$p = new Post;

						 if(!isset($_GET['id']) || $_GET['id']==NULL ||$_GET['id']<0)
							{
										header("Location:404.php");
							}
							else
							{

							$p->postId = $_GET['id'];


							//call the function for single post display

											foreach ($p->singlePostDisplay() as $item)
											{

														 $p->postId        =  $item->postId;
														 $p->postTitle     =  $item->postTitle;
														 $p->description   =  $item->description ;
														 $p->dateTime      =  $item->dateTime;







												}


							}//end of else section





							?>

								<div class="post_section"><!-- post will be placed here from database post table  -->


									<h2><?php echo $p->postTitle; ?></h2><!--post title-->
									<p class="post_date_time">

										<?php


										$date=date_create("$p->dateTime");
										echo '<span >'.date_format($date,"M j,Y" ).'</span>'.'<span>/<a href="aboutme.php">OMOUR</a></span>';





										?>




									</p>
									<div class="post-body"><!--start of post body section-->

											<p><?php echo  $p->description; ?></p><!--post body-->


									</div><!--end of post body section-->

								</div>








							<?php
							// retrive comments with post id
							include_once'db/Comments.php';


							$c = new Comments;

							$c->postId = $p->postId;




							?>



								<h2>Comments.....</h2>
								<div class="comment-block"><!--start of comment-block section-->

												<?php foreach ($c->commentDisplay() as $item): ?>
													<div class="comment-item">


																			<div class="comment-avatar">
																				<img src="image\profile.png" alt="profile-picture"><?php //$c->avatar($item->mail)?>
																			</div>



																		<div class="comment-post">
																			<h3><?php echo $item->name ?> <span>said....</span></h3>
																			<p><?php echo $item->comDescription ?></p>
																		</div>

													</div>
												<?php endforeach?>


								</div><!--End of comment-block section-->





								<h2>Submit new comment</h2>
								<!--comment form -->
								<form id="form" method="post" action="ajax_comment.php">


												<!-- need to supply post id with hidden fild -->
												<input type="hidden" name="postid" value="<?php echo $p->postId; ?>">


												<label>
													<span>Name *</span>
													<input type="text" name="name" id="comment-name" placeholder="Your name here...." required>
												</label>


												<label>
													<span>Email *</span>
													<input type="email" name="mail" id="comment-mail" placeholder="Your mail here...." required>
												</label>


												<label>
													<span>Your comment *</span>
													<textarea name="comment" id="comment" cols="30" rows="10" placeholder="Type your comment here...." required></textarea>
												</label>


												<input type="submit" id="submit" value="Submit Comment" onclick="myFunction()">


								</form>



		</div><!--End of wrap section-->



</div><!--end of body-post-comment-->

<div class="footer"><!---Start of the footer section -->


					<footer> copyright@2016</footer>


</div><!---End of the footer section -->




</body>
</html>
