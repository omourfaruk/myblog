

<!DOCTYPE html>

<html lang="en">
<head>
  <title>About Author</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/aboutme.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body >

<?php
//inclusion of menu-bar header section
 include_once'include/header.php';

 ?>


<div class="main-body" >

<div class="container">

  <!---start of the left_content section -->

      <div class="left_content">

        <p class="name">  Md. Omour Faruk </p>
        <p class="designation"> Programmer & Instrument Engineer</p>

        <div class="profile-section">

              <p class="profile"> Profile </p>

              <div class="eduacation">

                        <p class="edu-title">Education</p>


                      <p> <b>M.Sc.</b> in <b><a href="http://dept.ru.ac.bd/apee" target="_blank">Applied Physics & Electronic Engineering</a></b> from Rajshahi Universily of <b>class 2012</b> <p/>
                      <p> <b>B.Sc.</b> in <b ><a href="http://dept.ru.ac.bd/apee" target="_blank">Applied Physics & Electronic Engineering</a></b> from Rajshahi Universily of <b>class 2011</b> <p/>
                      <p> <b>H.S.C.</b> From <b > Shah Dowla College ,Bagha, Rajshahi</b> of <b>class 2007</b> <p/>
                      <p><b> S.S.C.</b> From <b >Shah Dowla College ,Bagha, Rajshahi </b> of <b>class 2005</b> <p/>

              </div><!--End of education section-->


              <div class="field-interest">

                <p class="fild-interest">Field of Interests</p>

                <p> *Programming</p>
                <p>* Instrumentation</p>
                <p>* Electronics</p>
                <p>* Mechatronics</p>
                <p>* Quantum Mechanics</p>
                <p>* Reading Books</p>
                <p>* Travelling </p>
                <p>* Valunteery Activites </p>

              </div><!--End of field-interest section-->






        </div><!--End of profile section-->




      </div>

    <!---End of the left_content section -->





              <div class="right_content">

                      <div class="photo-background">

                          <div class="photo">
                            <img src="image/omour.jpg" style="  ">

                          </div>



                          <div class="skill">

                            <span>Skills</span>

                          </div>



                                <br/><div class="progress"><!--start of progress bar-->
                                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40"
                                     aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                        HTML5 80%
                                   </div>
                               </div><!--end of progress bar-->


                               <div class="progress">
                                           <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                             CSS3 80%
                                            </div>
                              </div>


                              <div class="progress">

                                <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                  jQuery 50%
                                </div>

                              </div>


                              <div class="progress">

                                <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                                  Bootstrap 70%
                                </div>

                              </div>


                              <div class="progress">

                                <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                  JavaScript 50%
                                </div>

                              </div>





                            <div class="progress">

                                <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                    AJAX 50%
                                </div>

                             </div>


                             <div class="progress">

                                 <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                     PHP 80%
                                 </div>

                              </div>


                              <div class="progress">

                                  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                      MySQL 80%
                                  </div>

                               </div>


                      </div><!--End of photo-background-->


              </div><!--End of right content-->




</div>



<div>


<!---Start of the footer section -->


<div class="footer">

<footer> copyright@2016</footer>

</div>

<!---End of the footer section -->



</body>
</html>
