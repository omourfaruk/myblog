


<!DOCTYPE html>

<html lang="en">
<head>
  <title>My Blog</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<?php
//inclusion of menu-bar header section
 include_once'include/header.php';

 ?>


<div class="main-body">

            <div class="container">



                  <div class="left_content"><!---start of the left_content section -->



                      <?php
                        include_once 'db/Post.php';

                        $p = new Post();

                        if(isset($_GET['offset']))
                        {
                          $p->offset = $_GET['offset'];  //from where the data retriving should be start
                        //initially i.e. when the page is load for first time the value of $offset = 0 ;

                        }




                        foreach ($p->PaginationListDisplay() as $item)
                        {
                            print'<div class="post">

                            <div class="title"><a href="post_comment.php?id='.$item->postId.'">'.$item->postTitle.'</a></div>

                            <div class="data_time">'.'<span class="date_time">'. date_format(date_create("$p->dateTime"),"M j,Y ").
                            '</span>'.'<span class="author">/<a href="aboutme.php">OMOUR</a></span>'.'</div><br/>

                            <div class="description">'.$p->postShorten($item->description).'</div>


                            <div class="readore">

                              <button type="button"><a href=" post_comment.php?id='.$item->postId.'" >Read more</a> </button>

                            </div><br/><br/><br/><br/><br/><br/>


                          </div>  ';

                        }



                        ?>





                      <?php   //<!---start of the pagination section -->


                      $i= 0;
                      $j= 1;
                      print '<nav>
                        <ul class="pagination">';

                        if($p->offset != 0) //i.e. when we are not in the first page this loop will not work i.e. the Previous page sign will not appear
                        {
                      	print '
                          <li>
                            <a href="?offset='.(  $p->offset-$p->articlesPerPage).'" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                            </a>
                          </li>';
                        }


                      while($i*$p->articlesPerPage   < $p->totalArticles)
                      {
                      	print '<li><a href="?offset='.($i* 5).'">'.$j++.'</a></li>'; //$j++ is the post increment operator it first return $j then increment
                      	$i++;                                                          //by one
                      }


                      if(($p->offset + $p->articlesPerPage)  <  $p->totalArticles - ( $p->totalArticles % $p->articlesPerPage))
                      {
                      	print '
                          <li>
                            <a href="?offset='.($p->offset+$p->articlesPerPage).'" aria-label="Next">
                              <span aria-hidden="true">&raquo;</span>
                            </a>
                          </li>';

                      }





                       print ' </ul>
                      </nav>
                      ';


                      ?><!---End of the pagination section -->


                  </div><!---End of the left_content section -->







                  <div class="right_content">



                  </div>




            </div><!--End of container section-->



<div><!--End of main-body section-->





  <div class="footer"><!---Start of the footer section -->


            <footer> copyright@2016</footer>


  </div><!---End of the footer section -->





</body>
</html>
