

<!DOCTYPE html>

<html lang="en">
<head>
  <title>My Blog</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/post_display.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<?php
//inclusion of menu-bar header section
 include_once'include/header.php';

 ?>


<div class="main-body"><!---start of the main-body section -->

<!---start of the container section -->

          <div class="container">

                <!---start of the left_content section -->

                                  <div class="left_content">

                                    <?php

                                    include_once'db/Post.php';

                                    $p = new Post;

                                    if(!isset($_GET['id']) || $_GET['id']==NULL ||$_GET['id']<0)
                                    {
                                      header("Location:404.php");
                                    }
                                    else
                                    {

                                        $p->postId = $_GET['id'];


                                      //call the function for single post display

                                                  foreach ($p->singlePostDisplay() as $item)
                                                  {
                                                      print'

                                                      <div class="title">'.$item->postTitle.'</div>

                                                      <div class="data_time">'.$item->dateTime.' </div><br/>

                                                      <div class="description">'.$item->description.'</div>


                                                      ';

                                                  }


                                    }//end of else section



                                    ?>


                                  </div>

                        <!---End of the left_content section -->






                      <!---start of the right_content section -->

                            <div class="right_content">

                            </div>

                      <!---End of the right_content section -->


          </div>

          <!---End of the container section -->

<div><!---End of the main-body section -->






<!---Start of the footer section -->
<div class="footer">

</div>

<!---End of the footer section -->



</body>
</html>
