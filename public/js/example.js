


        //For name validation

        $(document).ready(function(){

                $('#checkName').keyup(function(){

                    var checkName = $(this).val();
                    var length = $(this).val().length;
                    var validNames =/^[A-Za-z\s\.]+$/;

                    if(checkName == "")
                    {
                          $('#resultFullName').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i>Type your full name please.</span>');
                    }

                    else if(! validNames.test(checkName))
                    {
                          $('#resultFullName').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i>Invalid Input.</span>');
                    }

                    else {

                              if(length<2)
                              {
                                $('#resultFullName').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i>Name must be of atleast two characters.</span>');
                              }
                              else if(length>30)
                              {
                                $('#resultFullName').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i>Name must not be more than 30 characters.</span>');
                              }
                              else
                              {
                                $('#resultFullName').html('<span></span>');
                              }


                      }

                });

        });



        //For Email validation
        $(document).ready(function(){

              $('#checkEmail').keyup(function(){

                var checkEmail = $(this).val();
                var length      = $(this).val().length;
                var emailPattern = /^[A-Za-z0-9._-]+@[A-Za-z]+\.[A-Za-z]{1,10}$/;

                if(checkEmail == "")
                      {
                      	       $('#resultEmail').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Type Your Email Id.</span>');

                      }

                      else if(!emailPattern.test(checkEmail))
                      {
                      	       $('#resultEmail').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Invalid email id.</span>');
                      }

                      else{

                      	      $('#resultEmail').html('');
                      }


              });


        });





    //For password validation
    $(document).ready(function(){

      $('#checkPassword').keyup(function(){

      var checkPassword = $(this).val();
      var length = $(this).val().length;
      var passsPattern = /^[A-Za-z0-9]+$/;

              if(checkPassword == "")
              {
                     $('#resultPassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Type Your password</span>');
              }

              else if(!passsPattern.test(checkPassword))
              {
                     $('#resultPassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Invalid Password.</span>');
              }

              else
              {
                    if(length < 8)
                    {
                            $('#resultPassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Password size minimum 8 characters long.</span>');
                    }

                    else if(length >15)
                    {
                            $('#resultPassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i>Password size maximum 15 characters long.</span>');

                    }

                    else{

                           $('#resultPassword').html('');
                    }



              }

      });


    });


        //For rePassword validation
        $(document).ready(function(){

          $('#checkRePassword').keyup(function(){

          var checkRePassword = $(this).val();
          var length = $(this).val().length;
          var rePasssPattern = /^[A-Za-z0-9]+$/;

                  if(checkRePassword == "")
                  {
                         $('#resultRePassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Type Your password</span>');
                  }

                  else if(!rePasssPattern.test(checkRePassword))
                  {
                         $('#resultRePassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Invalid Password.</span>');
                  }

                  else
                  {
                        var firstPass =   $('#checkPassword').val();

                        if(firstPass != checkRePassword)
                        {
                            $('#resultRePassword').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Your password does not match.</span>');
                        }

                        else
                        {
                          $('#resultRePassword').html('');
                        }



                  }

          });


        });



        //For Contact validation
        $(document).ready(function(){

                  $('#checkContact').keyup(function(){

                  var checkContact = $(this).val();
                  var length = $(this).val().length;
                  var contactPattern = /^[0-9]+$/;

                          if(checkContact == "")
                          {
                                 $('#resultContact').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Type Your Contact number.</span>');
                          }

                          else if(!contactPattern.test(checkContact))
                          {
                                 $('#resultContact').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i>Invalid contact number!Only numeric digits are valid.</span>');
                          }

                          else
                          {

                            if(length !=11)
                            {
                                    $('#resultContact').html('<span class="error_message"><i class="fa fa-exclamation-triangle"></i> Your Contact size not more or less than 11 digit.</span>');
                            }

                            else{


                                $('#resultContact').html('');
                            }


                          }


                  });


        });
