


<link type="text/css" rel="stylesheet" href="css/register.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">




<script>
    function send(country_id)
    {
    	if(window.XMLHttpRequest)
    	{
    		xmlhttp = new XMLHttpRequest();
    	}
    	else
    	{
    		xmlhttp = new XMLHttpRequest(Microsoft.XMLHTTP);
    	}


    	xmlhttp.onreadystatechange = function()
    	{
    		if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
    		{
    			document.getElementById('cityByCountryId').innerHTML = xmlhttp.responseText;
    		}

    	}
    	xmlhttp.open("GET","ajax_register.php?q="+country_id,true);
    	xmlhttp.send();



}


</script>


<?php

session_start();


include_once'../db/Registration.php';

$r = new Register();

$err      = 0;
$eName    = "";
$eEmail   = "";
$ePass    = "";
$eRePass  = "";
$eContact = "";
$eAddress = "";
$eCountry = "";
$eCity    = "";


        if(isset($_POST['btn_register']))
        {

              $r->userName       = $_POST['user_name'];
              $r->userEmail      = $_POST['email'];
              $r->userPassWord   = $_POST['pass_word'];
              $r->userRePassWord = $_POST['re_pass_word'];
              $r->userContact    = $_POST['contact'];
              $r->userAddress    = $_POST['address'];
              $r->countryId      = $_POST['country_id'];
              $r->cityId         = $_POST['city_id'];


              if($r->userName == "")
              {
                $err++;
                $eName = '<span class="error_message">*Enter your name please.</span>';
              }

              if($r->userEmail == "")
              {
                $err++;
                $eEmail = '<span class="error_message">*Enter your email address please.</span>';
              }

              if($r->userPassWord == "")
              {
                $err++;
                $ePass = '<span class="error_message">*Type your password.</span>';
              }


              if($r->userRePassWord == "")
              {
                $err++;
                $eRePass = '<span class="error_message">*Retype your password.</span>';
              }



              if($r->userContact == "")
                {
                    $err++;
                    $eContact = '<span class="error_message">*Type your contact number.</span>';
                }



              if($r->userAddress == "")
                {
                    $err++;
                    $eAddress = '<span class="error_message">*Type your address details.</span>';
                }


                if($r->countryId <1 )
                  {

                      $err++;
                      $eCountry = '<span class="error_message">*Select your country.</span>';

                  }

                if($r->cityId <1 )
                  {

                      $err++;
                      $eCity = '<span class="error_message">*Select your city.</span>';

                  }



                if($err == 0)
                {

                        if($r->userRegistration())
                        {
                          echo '<span class="sucess">Registration sucessfully done.</span>';

                          header('Location:../index.php');

                        }

                        else
                        {
                          echo '<span class="error_message">Registration failed.</span>';
                          print $r->error;
                        }

                }


        }










?>






<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Registration Page</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  </head>
  <body>



    <nav class="navbar navbar-default"><!--start of header section-->

          <div class="container-fluid">

            <div class="navbar-header">
              <a class="navbar-brand" href="../index.php">My Blog</a>
            </div>



            <ul class="nav navbar-nav">


                        <li class="active"><a href="../index.php">Home</a></li>
                        <li><a href="../aboutme.php">About Me</a></li>

            </ul>

          </div>

    </nav><!--end of header section-->









<div class="main-container-registration"><!--start of main container-->

  <div class="container-registration"><!--start of Registration section-->

      <form action="" method="post">

          <h1>Registration Information</h1>

            <!--For name input fields-->
            <p style="font-family:sans-serif;">*Full Name(required)</p>
            <input type="text" name="user_name" size="50"  class="fullName" id="checkName" placeholder="Type your first name please.">
            <br/><?php echo $eName;?><br>
            <span id="resultFullName" ></span><br><br>


            <!--For Email input fields-->
            <p style="font-family:sans-serif;">*Email(requied)</p>
            <input type="text" name="email" size="50" class="email" placeholder="Type your email adress please." id="checkEmail">
            <br/><?php echo $eEmail; ?><br>
            <span id="resultEmail"></span><br><br>


            <!--For Password input fields-->
            <p style="font-family:sans-serif;">*Password(required)</p>
            <input type="password" name="pass_word" size="50" class="password" placeholder="Type your password please." id="checkPassword">
            <br/><?php echo $ePass; ?>
            <br><span id="resultPassword"></span><br><br>

            <!--For rePassword input fields-->
            <p style="font-family:sans-serif;">*Retype Password(required)</p>
            <input type="password" name="re_pass_word" size="50" class="rePassword" placeholder="Retype your password please." id="checkRePassword">
            <br/><?php echo $eRePass; ?>
            <br><span id="resultRePassword"></span><br><br>

            <p style="font-family:sans-serif;">*Contact(reuired)</p>
            <input type="text" name="contact" size="50" class="contact" placeholder="Type your contact number please." id="checkContact">
            <br/><?php echo $eContact; ?><br>
            <span id="resultContact"></span><br><br>


            <p style="font-family:sans-serif;">*Address(required)</p>
            <input type="text" name="address" size="50"  class="address" placeholder="Type your address please.">
            <br/><?php echo $eAddress; ?><br><br>



            <p style="font-family:sans-serif;">*Country(required)</p>

            <!--start of Country Select OPtion------------------------------->
                          <select name="country_id" onchange="send(this.value)" class="countryList">

                            //for selecting Country Name
                            <?php

                            include_once'../db/Country.php';

                            $c = new Country;

                            print $c->countryList();

                            ?>

                          </select><?php echo $eCountry; ?>
                          <br><br>
                <!--End of Country Select OPtion------------------------------->


                <p style="font-family:sans-serif;">*City(required)</p>

  <!--Start of city name select option -->
                <select name="city_id" class="cityList" id="cityByCountryId">

                  <?php echo '<option value="0">Select One</option>'; ?>

                </select><?php echo $eCity ;?>

  <!--End of city name select option -->
                <br><br>

            <input type="submit" name="btn_register" value="Submit" class="registerBtn"><br><br>


      </form>

  </div><!--end of container-registration section--><!--end of Registration section-->



</div><!--end of main container-->



    <!---Start of the footer section -->


    <div class="footer-registration">

    <footer> copyright@2016</footer>

    </div>

    <!---End of the footer section -->




    <script src="js/jquery.js"></script>
    <script src="js/example.js"></script>




  </body>
</html>
